# Overview #

This is automation test script repository. This project use Katalon Studio to create and run the test scripts

### Installation ###
* Open https://www.katalon.com/
* Create account for Katalon
* Download the latest version of Katalon Studio
* Extract downloaded file
* Open katalon.exe in extracted folder
* Login with your Katalon account when application launched


### How To Run Test? ###

* Open test suit