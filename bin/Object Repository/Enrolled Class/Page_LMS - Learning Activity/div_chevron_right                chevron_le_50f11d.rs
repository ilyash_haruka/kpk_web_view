<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_chevron_right                chevron_le_50f11d</name>
   <tag></tag>
   <elementGuidId>b30d32e6-7312-4654-871e-1067a2b50b58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main']/div/div[4]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>main-content course-content</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
    
        
    
    
    
    

        
            
                
                    
                    
    
        
            
                chevron_right
                chevron_left
            
            
                
                Menjadi Barista dan Membuka Warung di Rumah
                13 April 2020 00:00 - 29 July 2020 23:59
            
            
                ▼1
                                        1
                                    
            
        
    

                    
                        Learning Objectives
                        
                            Menjadi Barista di Rumah
                        
                    

                    
                        Learning Materials
                        
                            
                                
                                
                                    Video 1 - Barista dan Sejarah Kopi
                                    
                                        
                                                                                                                                        29/07/2020
                                                                                    
                                    
                                
                            
                                
                                
                                    Video 2 - Jenis Proses Kopi dan Tanaman Kopi
                                    
                                        
                                                                                                                                        29/07/2020
                                                                                    
                                    
                                
                            
                                
                                
                                    Video 3 - Mengenal Basic Espresso dan Peralatannya
                                    
                                        
                                                                                                                                        29/07/2020
                                                                                    
                                    
                                
                            
                                
                                
                                    Video 4 - Pembuatan Espresso dan contoh Minuman Berbahan Espresso
                                    
                                        
                                                                                                                                        29/07/2020
                                                                                    
                                    
                                
                            
                                
                                
                                    Video 5 - Pembuatan Es Kopi Susu Gula Aren
                                    
                                        
                                                                                                                                        29/07/2020
                                                                                    
                                    
                                
                            
                                
                                
                                    Video 6 - Pembuatan Hot Latte dan Capuchino
                                    
                                        
                                                                                                                                        29/07/2020
                                                                                    
                                    
                                
                            
                                
                                
                                    Video 7 - Perhitungan Modal Secangkir Kopi
                                    
                                        
                                                                                                                                        29/07/2020
                                                                                    
                                    
                                
                            
                                
                                
                                    Video 8 - Tugas Praktik Mandiri
                                    
                                        
                                                                                                                                        29/07/2020
                                                                                    
                                    
                                
                            
                                
                                
                                    Quiz
                                    
                                        
                                                                                                                                        29/07/2020
                                                                                    
                                    
                                
                                                                        
                
            
        
    
</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main&quot;)/div[@class=&quot;page-content student&quot;]/div[@class=&quot;main-content course-content&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main']/div/div[4]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Live Session'])[2]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Message'])[2]/following::div[5]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]</value>
   </webElementXpaths>
</WebElementEntity>
