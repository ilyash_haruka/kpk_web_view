<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Video 6 - Pembuatan Hot Latte dan Capuchino</name>
   <tag></tag>
   <elementGuidId>fc360d82-0342-4cfe-8e3b-e134da997cb0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul/li[6]/a[@href='https://harukaedu-lms.harukaeduapps.com/public/learning_activity/1152/sections/5082/units/20487' and contains (text(),'Video 6 - Pembuatan Hot Latte dan Capuchino')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://harukaedu-lms.harukaeduapps.com/public/learning_activity/1152/sections/5082/units/20487</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Video 6 - Pembuatan Hot Latte dan Capuchino</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-sort</name>
      <type>Main</type>
      <value>6</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Video 6 - Pembuatan Hot Latte dan Capuchino</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;learning-activity&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col s12&quot;]/div[@class=&quot;panel&quot;]/div[@class=&quot;panel-body&quot;]/section[@class=&quot;full&quot;]/div[@class=&quot;scrolling-tab section-unit-tab&quot;]/div[@class=&quot;tab-wrap tab-wrap-section-unit&quot;]/ul[@class=&quot;tab-link main-tab&quot;]/li[@class=&quot;tab passed&quot;]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='learning-activity']/div/div[2]/div/div/section/div/div/ul/li[6]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Video 6 - Pembuatan Hot Latte dan Capuchino')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video 5 - Pembuatan Es Kopi Susu Gula Aren'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video 4 - Pembuatan Espresso dan contoh Minuman Berbahan Espresso'])[1]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video 7 - Perhitungan Modal Secangkir Kopi'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video 8 - Tugas Praktik Mandiri'])[1]/preceding::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Video 6 - Pembuatan Hot Latte dan Capuchino']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://harukaedu-lms.harukaeduapps.com/public/learning_activity/1152/sections/5082/units/20487')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div/ul/li[6]/a</value>
   </webElementXpaths>
</WebElementEntity>
