<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Video 2 - Jenis Proses Kopi dan Tanaman Kopi</name>
   <tag></tag>
   <elementGuidId>cf1bf840-3ed3-442a-8081-056f5da53de5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//ul/li[2]/a[@href='https://harukaedu-lms.harukaeduapps.com/public/learning_activity/1152/sections/5082/units/20491' and contains (text(),'Video 2 - Jenis Proses Kopi dan Tanaman Kopi')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://harukaedu-lms.harukaeduapps.com/public/learning_activity/1152/sections/5082/units/20491</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Video 2 - Jenis Proses Kopi dan Tanaman Kopi</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-sort</name>
      <type>Main</type>
      <value>2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Video 2 - Jenis Proses Kopi dan Tanaman Kopi</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;learning-activity&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col s12&quot;]/div[@class=&quot;panel&quot;]/div[@class=&quot;panel-body&quot;]/section[@class=&quot;full&quot;]/div[@class=&quot;scrolling-tab section-unit-tab&quot;]/div[@class=&quot;tab-wrap tab-wrap-section-unit&quot;]/ul[@class=&quot;tab-link main-tab&quot;]/li[@class=&quot;tab passed&quot;]/a[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='learning-activity']/div/div[2]/div/div/section/div/div/ul/li[2]/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Video 2 - Jenis Proses Kopi dan Tanaman Kopi')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video 1 - Barista dan Sejarah Kopi'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='chevron_right'])[3]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video 3 - Mengenal Basic Espresso dan Peralatannya'])[1]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Video 4 - Pembuatan Espresso dan contoh Minuman Berbahan Espresso'])[1]/preceding::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Video 2 - Jenis Proses Kopi dan Tanaman Kopi']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://harukaedu-lms.harukaeduapps.com/public/learning_activity/1152/sections/5082/units/20491')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//section/div/div/ul/li[2]/a</value>
   </webElementXpaths>
</WebElementEntity>
